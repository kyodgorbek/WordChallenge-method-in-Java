# WordChallenge-method-in-Java



import java.io.*;
import java.util.*;

public class WordChallenge {
   
     public static void main(String[] args) throws FileNotFoundException {
      System.out.println("Welcome to Scrabble word challenge!");
      
      // read sorted dictionary file not into a list
      Scanner in = new Scanner (new File("words.txt"));
      List<String> words = new ArrayList<String>();
      while (in.hasNext()){
        String word = in.next();
        words.add(word);
       
     }
     
     //binary search the list words
     Scanner console = new Scanner(System.in);
     while (true) {
      System.out.print("Word to challenge (Enter to quit)? ");
      String target = console.nextLine();
      if(target.trim().length() == 0 ) {
         break;
       }
       
       int index = Collections.binarySearch(words, target);
        if(index >= 0){
       System.out.println("\"" + target + "\" is word #" + index + " of " + words.size());
    } else {
      System.out.println(target + " is not found ");      
        
    }
   }
  }      
}    
